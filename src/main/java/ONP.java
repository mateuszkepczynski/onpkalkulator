import java.util.Stack;

public class ONP {
    private String equation;

    public ONP(String equation) {
        this.equation = equation;
    }

    private boolean is_operand(String token) {
        if (token.equals("+") || token.equals("-")
                || token.equals("*") || token.equals("/")
                || token.equals("(") || token.equals(")")) {
            return false;
        }
        return true;
    }

    private int getPriority(String token) {
        if (token.equals("*") || token.equals("/")) {
            return 2;
        } else if (token.equals("-") || token.equals("+")) {
            return 1;
        }
        System.err.println("Token with priority 0?");
        return 0;
    }

    public String convertToONP() {
        String onpEquation = "";
        Stack<String> stack = new Stack<String>();
        String[] tokens = equation.split(" ");
        for (String token : tokens) {
            if (is_operand(token)) {
                onpEquation += token + " ";
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.isEmpty()) {
                    String tokenFromStack = stack.pop();
                    if (tokenFromStack.equals("(")) {
                        break;
                    } else {
                        onpEquation += tokenFromStack + " ";
                    }
                }
            } else {
                int priority = getPriority(token);
                while (!stack.isEmpty()) {
                    String tokenFromStack = stack.pop();
                    if (tokenFromStack.equals("(") || getPriority(tokenFromStack) < priority) {
                        stack.push(tokenFromStack);
                        break;
                    }
                    onpEquation += tokenFromStack + " ";
                }
                stack.push(token);
            }
        }
        while (!stack.isEmpty()) {
            String item = stack.pop();
            onpEquation += item + " ";
        }
        return onpEquation.substring(0, onpEquation.length() - 1);
    }

    public double countONP(String equation) {
        Stack<String> stack = new Stack<String>();

        String[] tokens = equation.split(" ");
        for (String token : tokens) {
            if (is_operand(token)) {
                stack.push(token);
            } else {
                String op1 = stack.pop();
                String op2 = stack.pop();
                Double wynik = oblicz(op1, op2, token);
                stack.push(wynik.toString());
            }
        }
        Double wynik = Double.parseDouble(stack.pop());
        return wynik;
    }

    private Double oblicz(String op1, String op2, String token) {
        if (token.equals("*")) {
            return Double.parseDouble(op1) * Double.parseDouble(op2);
        } else if (token.equals("+")) {
            return Double.parseDouble(op1) + Double.parseDouble(op2);
        } else if (token.equals("-")) {
            return Double.parseDouble(op2) - Double.parseDouble(op1);
        } else if (token.equals("/")) {
            return Double.parseDouble(op1) / Double.parseDouble(op2);
        }
        System.err.println("Err");
        return 0.0;
    }
}
