import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame{
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Window().setVisible(true);
            }
        });
    }

    public Window() {
        setContentPane(mainPanel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        setPreferredSize(new Dimension(500,500));


        ActionListener listener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {

              Object source =  e.getSource();

              if (source instanceof JButton){
                  JButton button = (JButton) source;
                  String content = button.getText();

                  equation.setText((equation.getText()+content).replace("..","."));
              }

            }
        };
        ActionListener listener_operators = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Object source =  e.getSource();

                if (source instanceof JButton){
                    JButton button = (JButton) source;
                    String content = button.getText();
                    equation.setText(equation.getText()+" "+content+" ");
                }

            }
        };
        a0Button.addActionListener(listener);
        a1Button.addActionListener(listener);
        a2Button.addActionListener(listener);
        a3Button.addActionListener(listener);
        a4Button.addActionListener(listener);
        a5Button.addActionListener(listener);
        a6Button.addActionListener(listener);
        a7Button.addActionListener(listener);
        a8Button.addActionListener(listener);
        a9Button.addActionListener(listener);
        buttonDecimal.addActionListener(listener);

        //Operatory
        buttonAdd.addActionListener(listener_operators);
        buttonSub.addActionListener(listener_operators);
        buttonMulti.addActionListener(listener_operators);
        buttonDep.addActionListener(listener_operators);
        buttonLB.addActionListener(listener_operators);
        buttonRB.addActionListener(listener_operators);
        //Clear
        CEButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                equation.setText("");
            }
        });
        //Remove one sign
        buttonBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(!equation.getText().isEmpty()){
                    equation.setText(equation.getText());
                    equation.setText(equation.getText().trim().substring(0,equation.getText().length()-1).trim());
                }
            }
        });
        //=
        buttonEquel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ONP onp = new ONP(equation.getText().trim());
                Double wynik = onp.countONP(onp.convertToONP());
                equation.setText(wynik.toString());

            }
        });

        pack();
    }

    private JPanel mainPanel;
    private JPanel northPanel;
    private JPanel southPanel;
    private JTextField equation;
    private JPanel leftPanel;
    private JPanel rightPanel;
    private JPanel leftInternalPanel;
    private JPanel rightInternalPanel;
    private JButton a8Button;
    private JButton a9Button;
    private JButton a7Button;
    private JButton a4Button;
    private JButton a2Button;
    private JButton buttonDecimal;
    private JButton a5Button;
    private JButton a6Button;
    private JButton a1Button;
    private JButton a3Button;
    private JButton a0Button;
    private JButton buttonMulti;
    private JButton buttonAdd;
    private JButton buttonEquel;
    private JButton buttonDep;
    private JButton buttonLB;
    private JButton buttonSub;
    private JButton buttonRB;
    private JButton CEButton;
    private JButton buttonBack;

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
